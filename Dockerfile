FROM debian:buster

RUN apt-get update && apt-get upgrade -y && apt-get install -y --no-install-recommends \
    ca-certificates \
    build-essential \
    cmake \
    gettext \
    git-core \
    gpsd \
    gpsd-clients \
    libgps-dev \
    wx-common \
    libwxgtk3.0-dev \
    libwxgtk3.0-gtk3-dev \
    libwxgtk-webview3.0-gtk3-dev \
    libglu1-mesa-dev \
    libgtk-3-dev \
    wx3.0-headers \
    libbz2-dev \
    libtinyxml-dev \
    libportaudio2 \
    portaudio19-dev \
    libcurl4-openssl-dev \
    libexpat1-dev \
    libcairo2-dev \
    libarchive-dev \
    liblzma-dev \
    libexif-dev \
    libelf-dev \
    libsqlite3-dev \
    libsndfile1-dev \
    libjpeg-dev \
    && rm -rf /var/lib/apt/lists/*
